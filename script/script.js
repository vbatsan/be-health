$(document).ready(function () {
    if (localStorage.getItem('cards')) {
        $('#board').html(localStorage.getItem('cards'));
    }

});

$('#header-btn').click(function () {
    $('#backk').removeClass('hide-field');
    $('.cardio').removeClass('hide-field')
    $('#modal-task').removeClass('hide-field');
    $('.cardio').prop('disabled', false)
});

$('#modal-cross').click(function () {
    $(this).closest('div').addClass('hide-field');
    $('#set-task-form').trigger("reset");
    $('.data-fields').addClass('hide-field')
    const back = $('#backk');
    if (!back.hasClass('hide-field')) {
        $('#backk').addClass('hide-field');
    }
});

$("#backk").click(function (e) {
    if ($("#backk").has(e.target).length === 0) {
        $("#backk").addClass('hide-field')
        $('#set-task-form').trigger("reset");
        $('.data-fields').addClass('hide-field');

    }
});

$('#board').on('click', '#card-cross', function () {
    $(this).closest('div').detach();
    if ($('.card').length === 0) {
        $('.task-board-descr:hidden').show()
        $('#task-board').removeClass('container--flex')
    }
    const card = $('#board').html();
    localStorage.setItem('cards', card);
});
$('#board').on('click', '.card-btn', function (){
    $(this).closest('div').children('.addition-prop').removeClass('hide-field');
    $(this).detach()
});



$('#doctors').change(function () {
    if (this.value === 'Кардиолог') {
        $('.data-fields').addClass('hide-field');
        $('.cardio').removeClass('hide-field');
        $('.data-fields').prop('disabled', true);
        $('.cardio').prop('disabled', false);
        return;
    }
    if (this.value === "Стоматолог") {
        $('.data-fields').addClass('hide-field');
        $('.stomat').removeClass('hide-field');
        $('.data-fields').prop('disabled', true);
        $('.stomat').prop('disabled', false);
        return;
    }
    if (this.value === "Терапевт") {
        $('.data-fields').addClass('hide-field');
        $('.terap').removeClass('hide-field');
        $('.data-fields').prop('disabled', true);
        $('.terap').prop('disabled', false);
    }
});

$('#set-task-form').submit(function () {
    const visit = $('#doctors').val();
    const date = $('#visit-date').val();
    const fullName = $('#fullName').val();
    const target = $('#target').val();
    const pressure = $('#pressure').val();
    const massIndex = $('#mass-index').val();
    const lastVisit = $('#date').val();
    const sick = $('#sick').val();
    const age = $('#age').val();
    const coment = $('#coments').val();

    $('.task-board-descr').hide();
    $('#backk').addClass('hide-field');
    $('#task-board').addClass('container--flex');
    $('#set-task-form').trigger("reset");
    $('.data-fields').addClass('hide-field');

    if (visit === 'Кардиолог') {
        new Cardio(fullName, visit, date, target, pressure, massIndex, sick, age, coment).createCard();
        return
    }
    if (visit === 'Терапевт') {
        new Terap(fullName, visit, date, target, age, coment).createCard();
        return;
    }
    if (visit === 'Стоматолог') {
        new Stomat(fullName, visit, date, lastVisit, age, coment).createCard()
    }


});

class Visit {
    constructor(fullName, doctor, date) {
        this.doctor = {
            value: doctor,
            visible: true,
        };
        this.date = {
            value: date,
            visible: true
        }
        this.fullName = {
            value: fullName,
            visible: true
        };
    }

    createCard() {
        const div = document.createElement('div');
        for (let key in this) {
            if (this[key]['visible'] === true) {
                $(div).append(`<p class="card-prop">${this[key]['value']}</p>`)
            }
            if (this[key]['visible'] === false) {
                $(div).append(`<p class=" hide-field addition-prop card-prop">${this[key]['value']}</p>`)
            }
        }
        $(div).addClass('card');
        $(div).append('<span class="cross cross--color" id="card-cross"><i class="fas fa-times"></i></span><button class="card-btn">Показать больше</button>');
        if ($('.card').length) {$(div).css('left', $('.card').length * 200 + 10)}
        $('#task-board').append(div);
        const card = $('#board').html();
        localStorage.setItem('cards', card);

    }

}

class Cardio extends Visit {
    constructor(fullName, doctor, date, target, pressure, massIndex, lastAilments, age, coment) {
        super(fullName, doctor, date);
        this.visitTarget = {
            value: target,
            visible: false
        };
        this.pressure = {
            value: pressure,
            visible: false
        };
        this.massIndex = {
            value: massIndex,
            visible: false
        };
        this.lastAilments = {
            value: lastAilments,
            visible: false
        };
        this.age = {
            value: age,
            visible: false
        };
        this.coment = {
            value: coment,
            visible: false
        };
    }

}

class Terap extends Visit {
    constructor(fullName, doctor, date, target, age, coment) {
        super(fullName, doctor, date);
        this.visitTarg = {
            value: target,
            visible: false
        }
        this.age = {
            value: age,
            visible: false
        }
        this.coment = {
            value: coment,
            visible: false
        }
    }
}

class Stomat extends Visit {
    constructor(fullName, doctor, date, lastVisit, age, coment) {
        super(fullName, doctor, date);

        this.age = {
            value: age,
            visible: false
        };
        this.lastVisit = {
            value: lastVisit,
            visible: false
        }
        this.coment = {
            value: coment,
            visible: false
        }
    }
}

// DradAndDrop

let mouseOffset = {x:0, y: 0};
let isMouseDown = false;
let targ = null;
$('#board').on("mousedown", '.card-prop',  function (e) {

    targ = e.target.closest('div');
    isMouseDown = true;
    mouseOffset = {x: targ.offsetLeft - e.clientX, y: targ.offsetTop - e.clientY};

});

$('body').on('mousemove', function (e) {
    e.preventDefault();
    if(isMouseDown) {
        $(targ).css('z-index',2);
        $(targ).css('left', e.clientX + mouseOffset.x + "px");
        $(targ).css('top', e.clientY + mouseOffset.y + 'px');
       setDragArea($(targ), $('#task-board'))
    }
});

$('body').mouseup( function () {
    isMouseDown = false;
    $(targ).css('z-index', 0);
    const card = $('#board').html();
    localStorage.setItem('cards', card);
});

function setDragArea(elem, parent) {
    if ($(elem).offset().top  < $(parent).offset().top +10) {
        $(elem).offset({top:$(parent).offset().top + 10})
    }
    if ($(elem).offset().left  < $(parent).offset().left +10) {
        $(targ).offset({left:$(parent).offset().left + 10})
    }
    if ($(elem).offset().top + $(elem).height() > $(parent).offset().top +$(parent).height() -10) {
        $(elem).offset({top:$(parent).offset().top + $(parent).height() - 10 - $(elem).height()})
    }
    if ($(elem).offset().left + $(elem).width() > $(parent).offset().left +$(parent).width() -10) {
        $(elem).offset({left:$(parent).offset().left + $(parent).width() - 10 - $(elem).width()})
    }

}